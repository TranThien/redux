import { listData } from "../../data";

const initialState = {
  data: listData,
  detail: {},
  cart: [],
};

export const shoeShopReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_TO_CART": {
      let cloneCart = [...state.cart];
      const index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });

      if (index == -1) {
        // nếu chưa có trong giỏ hàng
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case "TANG_SO_LUONG": {
      console.log(action.payload);
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload;
      });
      if (index !== -1) {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case "GIAM_SO_LUONG": {
      console.log(action.payload);
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        console.log(action.payload.id);
        return item.id == action.payload;
      });
      if (index !== -1 && cloneCart[index].number > 0) {
        cloneCart[index].number--;
      }
      if (cloneCart[index].number == 0) {
        alert("You definitely want to delete");
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    case "DELETE": {
      let cartItem = [...state.cart];
      const index = cartItem.findIndex((item) => {
        return item.id == action.payload;
      });
      if (index !== -1) {
        cartItem.splice(index, 1);
      }
      return { ...state, cart: cartItem };
    }
    case "DETAIL": {
      return { ...state, detail: action.payload };
    }
    default:
      return state;
  }
};
