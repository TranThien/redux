import React, { Component } from "react";
import { connect } from "react-redux";

class ItemShoe extends Component {
  render() {
    const { id, name, price, description, image } = this.props.data;
    // lấy dữ liệu truyền từ cha là listShoe truyền qua
    return (
      <div className="col-4 mt-3">
        <div className="card h-100 " style={{ width: "100%" }}>
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">Tên SP :{name}</h5>
            <h3 className="card-title">Giá: {price}$</h3>
            <p className="card-text">Giới thiệu :{description}</p>
          </div>
          <div>
            <button
              onClick={() => {
                this.props.handleDetailProduct(this.props.data);
              }}
              className="btn btn-primary mx-2 mb-2"
            >
              Xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-info mx-2 mb-2"
            >
              Thêm vào giỏ
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      let action = {
        type: "ADD_TO_CART",
        //khi truyền dữ liệu vào func thì cần payload
        payload: shoe,
      };
      return dispatch(action);
    },
    handleDetailProduct: (shoe) => {
      let action = {
        type: "DETAIL",
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
