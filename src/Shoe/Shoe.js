import React, { Component } from "react";
import Cart from "./Cart";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Shoe extends Component {
  render() {
    return (
      <div>
        <Cart />
        <div className="container">
          <ListShoe />
        </div>
        <DetailShoe />
      </div>
    );
  }
}
