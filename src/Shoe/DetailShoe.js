import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    const { id, name, price, description, image, quantity } = this.props.detail;
    return (
      <div className="row container">
        <div className="col-4">
          <img src={image} alt="" width="300px" />
        </div>
        <div className="col-8">
          <h4>{id}</h4>
          <h3>{name}</h3>
          <h2>{price}</h2>
          <p>{description}</p>
          <h3>{quantity}</h3>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.shoeShopReducer.detail,
  };
};

export default connect(mapStateToProps)(DetailShoe);
