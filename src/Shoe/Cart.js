import React, { Component } from "react";
import { connect } from "react-redux";

class Cart extends Component {
  renderCart = () => {
    return this.props.dataCart.map((cart) => {
      return (
        <tr>
          <td>{cart.id}</td>
          <td>{cart.name}</td>
          <td>
            <img src={cart.image} width={60} alt="" />
          </td>
          <td>{cart.price * cart.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleReduceItemCart(cart.id);
              }}
              className="btn btn-success"
            >
              -
            </button>
            <span className="mx-2">{cart.number}</span>
            <button
              onClick={() => {
                this.props.handleRaiseItemCart(cart.id);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemoveItemCart(cart.id);
              }}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>Mã sản phẩm</th>
              <th>Tên sản phẩm</th>
              <th>Hình ảnh</th>
              <th>Giá</th>
              <th>Số lượng</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    dataCart: state.shoeShopReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleReduceItemCart: (id) => {
      let action = {
        type: "GIAM_SO_LUONG",
        payload: id,
      };
      dispatch(action);
    },
    handleRaiseItemCart: (id) => {
      let action = {
        type: "TANG_SO_LUONG",
        payload: id,
      };
      dispatch(action);
    },
    handleRemoveItemCart: (id) => {
      let action = {
        type: "DELETE",
        payload: id,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
