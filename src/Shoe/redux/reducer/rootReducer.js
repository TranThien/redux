import { combineReducers } from "redux";
import { shoeShopReducer } from "./shoeShop_Reducer";

export const rootReducer_ShoeShopReducer = combineReducers({
  shoeShopReducer,
});
