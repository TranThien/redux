import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListItem = () => {
    return this.props.listData.map((item) => {
      return <ItemShoe data={item} key={item.id} />;
    });
  };
  render() {
    return <div className="row">{this.renderListItem()}</div>;
  }
}
// mapStateToProps là phương thức lấy dữ liệu từ Store về
let mapStateToProps = (state) => {
  return {
    listData: state.shoeShopReducer.data,
  };
};
export default connect(mapStateToProps)(ListShoe);
